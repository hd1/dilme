import binascii
import hashlib
import logging
import os
import secrets
from struct import pack
import sys
import time
import unittest

import pg8000
from sqlalchemy import CheckConstraint, Column, Float, ForeignKey, Integer, MetaData, String, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, validates
from sqlalchemy import create_engine
from validators import email, url
 

Base = declarative_base()


class Attribute(Base):
    __tablename__ = 'attributes'
    attribute_id = Column(Integer, primary_key=True, nullable=False)

    attr_name = Column(String(255), nullable=False)
    attr_value = Column(String(255), nullable=False)

    user_id = Column(Integer, ForeignKey('users.user_id'))

    # add other chains in the same way to validate other keys
    @validates('value')
    def validate_attribute(self, key, value):
        if key == 'photo':
            try:
                url(value)
            except ValidationFailure:
                return False

class User(Base):
    __tablename__ = 'users'
    __tableargs__ = (CheckConstraint('expires_at > {0}'.format(time.time())),)
    
    user_id = Column(Integer, primary_key=True, nullable=False)

    password_hash = Column(String(64), nullable=False)

    # Facebook authentcation information, pull profile data as-needed
    last_access_token = Column(Text, nullable=True)
    refresh_token = Column(Text, nullable=True)
    account_secret = Column(Text, nullable=False)
    expires_at = Column(Float, nullable=True)
    uid = Column(Integer, nullable=False)

    attributes = relationship('Attribute', lazy='dynamic')

    # TODO Figure out how to set name to the user's login if blank
    @staticmethod
    def blowfish(name, password):
        password_and_salt = '{0}{1}'.format(password, name)
        ret = hashlib.sha256(password_and_salt.encode('utf-8')).hexdigest()
        return ret

class Message(Base):
    __tablename__ = 'messages'

    __table_args__ = (CheckConstraint('sender != recipient'),)

    message_id = Column(Integer, primary_key=True, nullable=False)

    subject = Column(String(255), nullable=False)
    text = Column(Text, nullable=False)

    sender = Column(Integer, ForeignKey('users.user_id'))
    recipient = Column(Integer, ForeignKey('users.user_id'))

class Votes(Base):
    __tablename__ = 'votes'
    __table_args__ = (CheckConstraint('voter != votee'),)

    vote_id = Column(Integer, primary_key=True, nullable=False)
    voter = Column(Integer, ForeignKey('users.user_id'))
    votee = Column(Integer, ForeignKey('users.user_id'))


class Tests(unittest.TestCase):
    def setUp(self):
        u1 = User(uid=827790433, password_hash=User.blowfish('foo', 'oof'), expires_at=time.time()+600, last_access_token='bxx0', account_secret='0xxb')

        u1.attributes = [Attribute(attr_name='employer', attr_value='Barcelona FC'), Attribute(attr_name='city', attr_value='Barcelona'), Attribute(attr_name='country', attr_value='Spain'), Attribute(attr_name='nationality', attr_value='.nl'), Attribute(attr_name='photo', attr_value='http://dailypost.ng/wp-content/uploads/2017/03/johan-cruyff_jq2.jpg')]

        session = Session()
        session.add(u1)
        session.commit()

    def tearDown(self):
        session = Session()
        u1 = session.query(User).filter_by(user_id=1).first()
        if u1 is not None:
            session.delete(u1)
        session.commit()

    def test_prevent_sending_messages_from_and_to_the_same_user(self):
        session = Session()
        u1 = session.query(User).filter_by(user_id=1).first()

        u1.attributes.append(Attribute(attr_name='email', attr_value='foo@mailinator.com'))
        m = Message(subject='hello world', text='This should not be allowed', sender=u1, recipient=u1)
        session.add(m)
        try:
            session.commit()
            self.fail()
        except:
            pass

    def test_pwhash(self):
        hashed = User.blowfish('test', 'test1')
        self.assertTrue(hashed == '4e4afef8b587fac78ec294c90b595ecdf2b63a1bbe5e6ba7b1035702664b7ba5')
        self.assertTrue(len(hashed) == 64)

    def test_vote_for_self_not_allowed(self):
        session = Session()
        u1 = u2 = session.query(User).filter_by(user_id=1).first()
        v = Votes(voter=u1, votee=u2)
        session.add(v)
        try:
            session.commit()
            fail('Votes should not be recursive')
        except:
            pass

if __name__=='__main__':
    logging.basicConfig(level=logging.DEBUG)

    url = 'postgresql+pg8000://pgsql:@127.0.0.1/dilme'
    engine = create_engine(url)
    Session = sessionmaker(bind=engine)
    session = Session()

    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

    # run tests
    unittest.main()
